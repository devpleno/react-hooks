import React, { useReducer } from 'react'

const reducer = (state, action) => {
    switch (action.type) {
        case "INCREMENT":
            return { counter: state.counter + action.valor }
        case "DECREMENT":
            return { counter: state.counter - action.valor }
        default:
            return state
    }
}

const UseReducer = (props) => {

    const [state, dispatch] = useReducer(reducer, { counter: 0 })
    const { counter } = state

    return (
        <div>
            <h2>UseReducer: {counter}</h2>

            <button onClick={() => dispatch({ type: 'DECREMENT', valor: 1 })}>-1</button>
            &nbsp;
            <button onClick={() => dispatch({ type: 'INCREMENT', valor: 2 })}>+2</button>
        </div>
    )
}

export default UseReducer