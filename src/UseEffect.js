import React, { useEffect, useState } from 'react'


const UseEffect = (props) => {

    const [counter, setCounter] = useState(0)

    useEffect(() => {
        console.log('useEffect-construtor')

        const timer = setInterval(() => {
            setCounter(old => old + 1)
        }, 1000)

        return () => {
            clearInterval(timer)
            console.log('useEffect-destroy')
        }
    }, [])

    return (
        <div>
            <h2>UseEffect: {counter}</h2>
        </div>
    )
}

export default UseEffect