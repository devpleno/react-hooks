import React, { useState } from 'react'

const UseState = (props) => {

    const [counter, setCounter] = useState(0)

    return (
        <div>
            <h2>UseState: {counter}</h2>

            <button onClick={() => setCounter(counter + 1)}>+1</button>
        </div>
    )
}

export default UseState