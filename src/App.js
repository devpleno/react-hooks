import React, { Component } from 'react';
import UseState from './UseState'
import UseReducer from './UseReducer';
import UseEffect from './UseEffect';
import UseContext from './UseContext';

class App extends Component {

  state = {
    show: false,
    counter: 0
  }

  render() {
    return (
      <div>
        <UseState />
        <hr />
        <UseReducer />
        <hr />

        <button onClick={() => this.setState({ show: !this.state.show })}>Clique aqui</button>

        {this.state.show &&
          <UseEffect counter={this.state.counter} />
        }

        <hr />
        <UseContext />
      </div>
    );
  }
}

export default App;
