import React, { useContext } from 'react'
import MyContext from './MyContext'

const D1 = (props) => {

    const value = useContext(MyContext)

    {/*<p>{props.counter}</p>*/ }

    return (
        <p>Valor: {value}</p>
    )
}

const UseContextDeep = (props) => {
    return (
        <div>
            {/*<D1 counter={props.counter} />*/}
            <D1 />
        </div>
    )
}

export default UseContextDeep