import React, { useContext, useState, useEffect } from 'react'
import UseContextDeep from './UseContextDeep'
import MyContext from './MyContext'

const UseContext = (props) => {

    const [counter, setCounter] = useState(0)

    useEffect(() => {
        console.log('useEffect-construtor')

        const timer = setInterval(() => {
            setCounter(old => old + 1)
        }, 1000)

        return () => {
            clearInterval(timer)
            console.log('useEffect-destroy')
        }
    }, [])

    return (
        <div>
            <h2>UseContext: </h2>

            <MyContext.Provider value={counter}>
                {/*<UseContextDeep counter={counter} />*/}
                <UseContextDeep />
            </MyContext.Provider>
        </div>
    )
}

export default UseContext